package com.yung.rpa.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OpenRobot extends GeneralScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, JScrollPane scrollPane, JTextArea log) throws Exception {

        logMessage(scrollPane, log, "start to open Chrome Rebot!");
        String num = prop.getProperty("robot.number");
        int robotNum = 1;
        if (num != null && !"".equals(num)) {
            robotNum = Integer.valueOf(num);
            if (robotNum < 1) {
                robotNum = 1;
            }
        }
        for (int i = 0; i < robotNum; i++) {
            openStream(driver, prop, scrollPane, log, (i + 1));
            logMessage(scrollPane, log, "Open " + (i + 1) + " Chrome Rebot!");
        }
        logMessage(scrollPane, log, "Done!");
        
    }

    private void openStream(WebDriver driver, Properties prop, JScrollPane scrollPane, JTextArea log, int idx) throws Exception {
        
        SimpleDateFormat sdf = new SimpleDateFormat("mmss");
        String regUrl = getConfig(prop, "reg.url");
        String seq = sdf.format(new Date());
        String username = System.getProperty("user.name");
        
        String name = username + seq;
        String email = username + seq + "@123.com";
        
        // Method 1.
        //regUrl = replaceAll(regUrl, "{{NAME}}", name);
        //regUrl = replaceAll(regUrl, "{{EMAIL}}", email);
        //logMessage(scrollPane, log, regUrl);
        
        //if (idx > 1) {
        //    ((JavascriptExecutor) driver).executeScript("window.open('" + regUrl + "')");
        //}
        //ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        //driver.switchTo().window(newTab.get(idx));
        //waitForText(driver, 30, "webinar");
        
        // Method 2.
        if (idx > 1) {
            ((JavascriptExecutor) driver).executeScript("window.open('" + regUrl + "')");
        }
        Thread.sleep(3000);
        switchTabByElement(driver, "//section//div//a[text()='Yes! Reserve my seat now']");
        WebElement ele = driver.findElement(By.xpath("//section//div//a[text()='Yes! Reserve my seat now']"));
        ele.click();
        Thread.sleep(3000);
        WebElement inputName = driver.findElement(By.xpath("//form//div//input[@name='NAME']"));
        inputName.sendKeys(name);
        WebElement inputEmail = driver.findElement(By.xpath("//form//div//input[@name='EMAIL']"));
        inputEmail.sendKeys(email);
        WebElement submit = driver.findElement(By.xpath("//form//div//input[@type='submit']"));
        submit.click();
        Thread.sleep(3000);
        try {
            WebElement enterWebinar = driver.findElement(By.xpath("//section//div//a[text()='Enter webinar']"));
            enterWebinar.click();
            Thread.sleep(15000);
            // close Congratulations page
            driver.close();
        } catch (Exception e) {
            // swallow exception
            Thread.sleep(15000);
        }
        // enter webinar
        switchTabByElement(driver, "//div//a[text()='Enter now']");
        WebElement enterTag = driver.findElement(By.xpath("//div//a[text()='Enter now']"));
        enterTag.click();
        
        Thread.sleep(5000);
        
    }

    
}
