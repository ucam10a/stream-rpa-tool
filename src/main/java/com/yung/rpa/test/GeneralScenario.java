package com.yung.rpa.test;

import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yung.web.test.Scenario;

public abstract class GeneralScenario extends Scenario {

    public abstract void execute(WebDriver driver, Properties prop, JScrollPane scrollPane, JTextArea log) throws Exception;
    
    public void run(WebDriver driver, Properties prop, JScrollPane scrollPane, JTextArea log) {
        try {
            execute(driver, prop, scrollPane, log);
        } catch (Exception e) {
            logMessage(scrollPane, log, e.toString());
            e.printStackTrace();
            setSTOP(true);
            driver.quit();
        }
    }
    
    protected String getConfig(Properties prop, String key) throws Exception {
        String result = prop.getProperty(key);
        if (result == null || "".equals(result)) {
            throw new Exception(key + " is null in properties");
        }
        return result;
    }
    
    public static void waitForText(WebDriver driver, int seconds, final String... msgs) {
        if (isSTOP()) throw new RuntimeException("Cancel process");
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                String text = d.findElement(By.xpath("//body")).getText();
                for (String msg : msgs) {
                    if (!text.contains(msg)) {
                        //System.out.println("msg: " + msg + ", not contains text: " + text);
                        return false;
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                return true;
            }
        };
        wait.until(condition);
    }
    
    public static void waitForAlert(WebDriver driver, WebElement submit, final String... texts) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                Alert alert = d.switchTo().alert();
                String msg = alert.getText();
                msg = msg.toLowerCase();
                for (String text : texts) {
                    if (!msg.contains(text.toLowerCase())) {
                        //System.out.println("msg: " + msg + ", not contains text: " + text);
                        d.switchTo().defaultContent();
                        return false;
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                alert.accept();
                d.switchTo().defaultContent();
                return true;
            }
        };
        clickElement(driver, submit);
        wait.until(condition);
    }
    
    public static void clickElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", element);
    }
    
    public static void waitForElement(WebDriver driver, String xpathExpression, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathExpression)));
    }
    
    public static void switchTabByElement(WebDriver driver, String xpathExpression) {
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        for (String tabId : newTab) {
            driver.switchTo().window(tabId);
            try {
                waitForElement(driver, xpathExpression, 2);
                break;
            } catch (Exception e) {
                // ignore
            }
        }
    }
    
    protected void openTab(WebDriver driver, String homeUrl, String text) {
        ((JavascriptExecutor) driver).executeScript("window.open('" + homeUrl + "')");
        waitForText(driver, 30, text);
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(0));
    }
    
    protected String replaceAll(String originalText, String key, String replaceValue){
        if (originalText == null || originalText.length() == 0 || key == null || key.length() == 0 ||
                replaceValue == null) return originalText;
        int fromIndex = 0, idx, len = key.length();
        StringBuffer sb = new StringBuffer();
        boolean found = false;
        while ((idx = originalText.indexOf(key, fromIndex)) != -1) {
            if (!found) found = true;
            sb.append(originalText.substring(fromIndex, idx));
            sb.append(replaceValue);
            fromIndex = idx + len;
        }
        if (!found) return originalText;
        if (fromIndex < originalText.length()) {
            sb.append(originalText.substring(fromIndex));
        }
        return sb.toString();
    }
    
}
