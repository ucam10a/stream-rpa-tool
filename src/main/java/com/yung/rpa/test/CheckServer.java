package com.yung.rpa.test;

import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.openqa.selenium.WebDriver;

public class CheckServer extends GeneralScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, JScrollPane scrollPane, JTextArea log) throws Exception {
        String homeUrl = getConfig(prop, "reg.url");
        goToPage(driver, homeUrl, "//body");
    }

}